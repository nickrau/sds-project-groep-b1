# SDS Project Groep B1
Het volgende project is de groepsopdracht voor het vak Software Defined Services gegeven op Zuyd Hogeschool.


## Verdeling repository
Omdat in dit project verschillende configuraties opgeslagen worden voor verschillende systemen worden deze per map opgeslagen.

## Container Registry
Hier zitten alle docker containers in dat gebruikt worden tijdens het project. Iedere container wordt hieronder beschreven.

### Azure-cli container met SPN login
Docker container waarmee toegang gekregen kan worden tot Azure middels Azure CLI.

1. Kan opgehaald worden met `docker pull registry.gitlab.com/nickrau/sds-project-groep-b1/azure-cli-spn`
2. Starten met `docker run -it -e AZURE_APPLICATION_ID=[Your Application ID] -e AZURE_SECRET=[Your Secret] -e AZURE_TENANT_ID=[Your Tenant ID] registry.gitlab.com/nickrau/sds-project-groep-b1/azure-cli-spn`
3. Na gebruik container stoppen met `docker stop [naam container]`.
4. Bij volgend gebruik opstarten met `docker start [naam container]` en vervolgens in container komen met `docker exec -it [naam container] bash`.

## De website
Om realisme te brengen in de omgeving is een webapp ontwikkeld genaamd Clickable. Deze webapp is omgezet in een Docker Container zodat deze in de omgeving opgezet kan worden.
De webapp is in een andere repository te vinden. De repository is via de volgende [link](https://gitlab.com/nickrau/clickabletestapp) te vinden.