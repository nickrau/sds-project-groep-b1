---

- block:
    - name: Making sure Ubuntu is up-to-date
      apt:
        upgrade: yes
        update_cache: yes
        cache_valid_time: 86400

    - name: Check if a reboot is needed for Debian and Ubuntu boxes
      register: reboot_required_file
      stat: path=/var/run/reboot-required get_md5=no

    - name: Reboot the Debian or Ubuntu server
      reboot:
        msg: "Reboot initiated by Ansible due to kernel updates"
        connect_timeout: 5
        reboot_timeout: 300
        pre_reboot_delay: 0
        post_reboot_delay: 30
        test_command: uptime
      when: reboot_required_file.stat.exists

    - name: Install latest version of all packages needed
      apt:
        name: "{{ item }}"
        state: latest
      loop: "{{ awx_needed_packages }}"

    - name: Install docker
      include_role:
        name: docker
      vars:
        docker_state: present

    - name: Install docker compose
      include_tasks: dockercompose.yml
      when: docker_compose is defined

    - name: Install pip packages
      command: pip3 install "{{ item }}"
      loop: "{{ pip3_packages }}"

    - name: Use npm to install npm
      command: npm install npm --global

    - name: Create directory ansible-awx
      file:
        path: /src/ansible-awx
        state: directory

    - name: Clone AWX Git repo
      get_url:
        url: https://github.com/ansible/awx/archive/17.1.0.zip
        dest: /src/ansible-awx/awx.zip

    - name: Unzip AWX
      unarchive:
        remote_src: yes
        src: /src/ansible-awx/awx.zip
        dest: /src/ansible-awx

    - name: Generate secret key
      set_fact:
        awx_secretkey: "{{ lookup('password', '/dev/null length=30 chars=ascii_letters') }}"
    - name: Show secret key
      debug:
        msg: "{{ awx_secretkey }}"

    - name: Replace inventory file with template
      template:
        src: templates/inventory.j2
        dest: /src/ansible-awx/awx-17.1.0/installer/inventory

    - name: Run installer playbook
      command: ansible-playbook -i inventory install.yml
      args:
        chdir: /src/ansible-awx/awx-17.1.0/installer/
  when: awx_install_state is defined and awx_install_state == "install"

- block:
    - name: Include Ansible Galaxy
      include_tasks: ansible-galaxy.yml
  when: galaxy_installer is defined